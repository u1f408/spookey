from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    install_requires=[
        "beautifulsoup4==4.9.1",
        "jinja2==2.11.2",
        "markdown==3.2.2",
        "markupsafe==1.1.1; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'",
        "pyyaml==5.3.1",
        "soupsieve==2.0.1; python_version >= '3.5'",
    ],
    name="spookey",
    version="0.1.0",
    author="Lauren Jenkinson",
    author_email="lauren@iris.ac.nz",
    description="A simple static site generator.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/alxce/spookey",
    packages=["spookey"],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.8",
)
