"""Site configuration.
"""

import yaml
from typing import Dict, Any


# TODO: add a command line option to generate a new site, dumping this config
# to the new _config.yml, since it's not actually exposed anywhere yet
DEFAULT_CONFIG = {
    "site_name": "Spookey Site",
    "baseurl": "https://example.com",
    "exclude": [
        ".git",
    ],
    "post": {
        "template": "post.html",
        "permalink": "/{year}/{slug}",
    },
    "page": {
        "template": "page.html",
    }
}


class Configuration:
    path: str
    data: Dict[str, Any]

    def __init__(self, path: str) -> None:
        self.path = path

        with open(self.path, 'rt') as fh:
            self.data = yaml.load(fh, Loader=yaml.SafeLoader)

    def __getitem__(self, key: Any) -> Any: 
        return self.data[key]


__all__ = [
    "DEFAULT_CONFIG",
    "Configuration",
]