"""Input rendering functions.

Custom rendering functions can be added to the RENDERERS dictionary to
register them with Spookey. Rendering functions must return a dictionary,
with three keys:

- "is_html" - is the rendered content HTML?
- "ext" - the file extension for the rendered data (including leading period)
- "out" - the rendered data
"""

import re
import os
import os.path
import subprocess

from spookey.exceptions import RenderException


MANDOC_RENDER_CMD = [
    "mandoc",
    "-K", "utf-8",
    "-T", "html",
    "-O", "fragment",
]


def _get_file_contents(file_name: str) -> str:
    """Get the contents of a file.
    """

    with open(file_name, 'rt') as fh:
        data = fh.read()

    return {
        "is_html": re.search("\.html?$", file_name) is not None,
        "ext": os.path.splitext(file_name)[1],
        "out": data,
    }


def render_mdoc(file_name: str) -> str:
    """Render an mdoc-formatted file to HTML.
    """

    output = subprocess.run(
        MANDOC_RENDER_CMD + [file_name],
        text=True,
        capture_output=True,
        check=False,
    )

    if output.returncode != 0:
        raise RenderException(f"{file_name}\nstdout={repr(output.stdout)} stderr={repr(output.stderr)}")

    return {
        "is_html": True,
        "ext": '.html',
        "out": output.stdout,
    }


def render_markdown(file_name: str) -> str:
    """Render a Markdown-formatted file to HTML.
    """

    import markdown

    content = _get_file_contents(file_name)
    output = markdown.markdown(
        content["out"],
        extensions=[
            "toc"
        ],
    )

    return {
        "is_html": True,
        "ext": '.html',
        "out": output,
    }


RENDERERS = {
    "\.[1-9]$": render_mdoc,
    "\.(?:md|markdown)$": render_markdown,
}


def render_file(file_name: str) -> str:
    """Render an input file to HTML, picking a renderer from the global list of
    renderers by matching on the file name. If no renderer was found, returns
    the raw file contents.
    """

    for regex in RENDERERS.keys():
        if re.search(regex, file_name) is not None:
            return RENDERERS[regex](file_name)

    return _get_file_contents(file_name)


__all__ = [
    "RENDERERS",
    "render_file",
    "render_mdoc",
    "render_markdown",
]
