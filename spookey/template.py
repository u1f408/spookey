"""Template helpers.
"""

from jinja2 import Template, Environment, FileSystemLoader, select_autoescape

class TemplateEnvironment:
    """A container for a Jinja environment.
    """

    path: str
    env: Environment


    def __init__(self, path: str) -> None:
        self.path = path
        self.env = Environment(
            loader=FileSystemLoader(self.path),
            autoescape=select_autoescape(['html', 'xml']),
        )


    def get_template(self, template: str) -> Template:
        """Get a template by name.
        """

        return self.env.get_template(template)


    def from_string(self, data: str) -> Template:
        """Get a template object from a string.
        """

        return self.env.from_string(data)
