from typing import Dict, Any

class RenderException(Exception):
    pass

__all__ = [
    "RenderException",
]