"""Representation of a site.
"""

from typing import List, Dict, Any

import re
import os
import os.path
import logging
from datetime import datetime
from bs4 import BeautifulSoup

from spookey.config import Configuration
from spookey.template import TemplateEnvironment
from spookey.render import render_file


POST_NAME_REGEXP = re.compile(r"^(\d{4}-\d{2}-\d{2})-([a-zA-Z0-9-]+)")

SITE_EXCLUDES = [
    "\\.git",
    "^_.",
]


class Site:
    base_path: str
    output_path: str
    config_path: str
    config: Configuration
    excludes: List[str]
    tpl_env: TemplateEnvironment
    pre_rendered: Dict[str, Dict[str, Any]]
    rendered: Dict[str, Dict[str, Any]]

    def __init__(self, base_path: str, output_path: str = None, config_path: str = None) -> None:
        self.base_path = base_path
        self.output_path = output_path
        if self.output_path is None:
            self.output_path = os.path.join(self.base_path, '_site')

        self.config_path = config_path
        if self.config_path is None:
            self.config_path = os.path.join(self.base_path, '_config.yml')

        self.config = None
        self.excludes = SITE_EXCLUDES
        self.tpl_env = None

        self.pre_rendered = {}
        self.rendered = {}


    def setup(self) -> None:
        logging.debug("Loading config and constructing template environment")

        self.config = Configuration(self.config_path)
        self.tpl_env = TemplateEnvironment(os.path.join(self.base_path, '_templates'))
        self.excludes += self.config["exclude"]


    def _post_permalink(self, date: Any, slug: str) -> str:
        params = {
            "date": date,
            "year": date.year,
            "month": date.month,
            "day": date.day,
            "slug": slug,
        }

        link = self.config["post"]["permalink"].format(**params)
        if not link.endswith(".html"):
            link = f"{link}.html"

        return link


    def _extract_title(self, html: str) -> str:
        soup = BeautifulSoup(html, 'html.parser')

        # mandoc check - if `.manual-text` exists, get contents of first .Nm
        # and contents of first .Nd, and return stripped strings of those
        # joined by an emdash
        if soup.find_all(class_='manual-text'):
            logging.debug('_extract_title: found mandoc container')

            name = soup.find(class_='Nm')
            desc = soup.find(class_='Nd')

            if name and desc:
                logging.debug("_extract_title: using mandoc .Nm and .Nd as title")

                return ' — '.join([
                    ' '.join(name.stripped_strings),
                    ' '.join(desc.stripped_strings),
                ])

        # otherwise, first h1, if present
        header = soup.find('h1')
        if header:
            logging.debug("_extract_title: Using first h1 as title")
            return ' '.join(header.stripped_strings)

        # if nothing, return empty string
        return None


    def _render_posts(self) -> None:
        base = os.path.join(self.base_path, "_posts")
        files = [f for f in os.listdir(base) if os.path.isfile(os.path.join(base, f))]
        files = [f for f in files if re.match(POST_NAME_REGEXP, f) is not None]
        files = [f for f in files if all([re.search(x, f) is None for x in self.excludes])]

        posts = []
        for fname in files:
            logging.debug("_render_posts: Render pass: %r", fname)

            match = re.match(POST_NAME_REGEXP, fname)
            date = datetime.strptime(match.group(1), '%Y-%m-%d')
            slug = match.group(2)
            permalink = self._post_permalink(date, slug)
            rendered = render_file(os.path.join(base, fname))

            self.pre_rendered[permalink] = {
                "type": "post",
                "permalink": permalink,
                "slug": slug,
                "date": date,
                "render": rendered,
            }


    def _render_pages(self) -> None:
        base = self.base_path
        files = [os.path.join(dp, f) for dp, dn, fn in os.walk(base) for f in fn]
        files = [os.path.relpath(f, start=base) for f in files if os.path.isfile(os.path.join(base, f))]
        files = [f for f in files if all([re.search(x, f) is None for x in self.excludes])]

        now = datetime.now()

        for fname in files:
            logging.debug("_render_pages: Render pass: %r", fname)

            slug = os.path.splitext(os.path.basename(fname))[0]
            rendered = render_file(os.path.join(base, fname))
            newpath = os.path.join('/', os.path.dirname(fname), f"{slug}") + rendered["ext"]
            permalink = newpath.replace('\\', '/')

            self.pre_rendered[permalink] = {
                "type": "page",
                "permalink": permalink,
                "slug": slug,
                "date": now,
                "render": rendered,
            }

    def _render_templates(self) -> None:
        self.rendered = {}
        pages = []

        for permalink in self.pre_rendered:
            data = self.pre_rendered[permalink]

            title = data["slug"]
            if data["render"]["is_html"]:
                _title = self._extract_title(data["render"]["out"])
                if _title is not None:
                    title = _title

                logging.debug("_render_templates: Title pass: %r", permalink)

            data["title"] = title
            pages.append(data)

        pages = sorted(pages, key=lambda k: k["date"])

        site_pages = {
            "posts": [f for f in pages if f["type"] == "post"],
            "pages": [f for f in pages if f["type"] != "post"],
        }

        for page in pages:
            permalink = page["permalink"]
            if not page["render"]["is_html"]:
                logging.debug("_render_templates: Template pass: %r is non-HTML, bypassing", permalink)

                self.rendered[page["permalink"]] = {
                    "data": page["render"]["out"],
                    "attrs": page,
                }

                continue

            logging.debug("_render_templates: Template pass (page as template): %r", permalink)

            page_tpldata = {
                "post": page,
                "site_pages": site_pages,
                "config": self.config,
            }

            page_content = self.tpl_env.from_string(page["render"]["out"]).render(page_tpldata)

            template = self.config["page"]["template"]
            if page["type"] == "post":
                template = self.config["post"]["template"]

            logging.debug("_render_templates: Template pass (main template): %r", permalink)

            template_data = {
                "post": page,
                "site_pages": site_pages,
                "content": page_content,
                "config": self.config,
            }

            rendered = self.tpl_env.get_template(template).render(template_data)

            logging.debug("_render_templates: Template pass done: %r", permalink)

            self.rendered[permalink] = {
                "data": rendered,
                "attrs": template_data,
            }


    def _write_rendered(self) -> None:
        if not os.path.isdir(self.output_path):
            logging.info("Creating output directory %s", self.output_path)
            os.mkdir(self.output_path)

        for path in self.rendered.keys():
            base = os.path.join(self.output_path, re.sub('^/', '', os.path.dirname(path)))
            if not os.path.isdir(base):
                os.mkdir(base)

            logging.debug("_write_rendered: Writing file: %s", path)
            fpath = os.path.join(base, os.path.basename(path))

            with open(fpath, 'w') as fh:
                fh.write(self.rendered[path]["data"])


    def build(self) -> None:
        logging.info("Starting build...")

        self._render_posts()
        self._render_pages()
        self._render_templates()

        logging.info("Writing to disk...")
        self._write_rendered()

        logging.info("Build complete.")


    def __repr__(self) -> str:
        return f"<Site path:{repr(self.base_path)}>"

__all__ = ["Site"]