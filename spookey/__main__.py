import logging
from argparse import ArgumentParser

from spookey.site import Site


def parse_args():
    parser = ArgumentParser(
        prog='spookey',
        description='A simple static site generator.',
        epilog="""The source code for this program is available at
        https://gitlab.com/alxce/spookey under the MIT License.
        """,
    )

    parser.add_argument(
        '--log-level',
        type=str,
        required=False,
        help='Level to set the logger to',
        default='INFO',
    )
    parser.add_argument(
        '--site-path',
        type=str,
        required=False,
        help='Path to the site to build',
        default='.',
    )
    parser.add_argument(
        '--output-path',
        type=str,
        required=False,
        help='Path to the output directory',
        default='./_site',
    )
    parser.add_argument(
        '--config',
        type=str,
        required=False,
        help='Path to configuration file, relative to site directory',
    )

    return parser.parse_args()


def main() -> None:
    args = parse_args()

    # set up logger
    numeric_level = getattr(logging, args.log_level.upper(), None)
    if isinstance(numeric_level, int):
        logging.basicConfig(level=numeric_level)
    else:
        logging.fatal("Invalid log level: %s", args.log_level)

    # create site instance
    site = Site(args.site_path, output_path=args.output_path, config_path=args.config)
    logging.debug("main: Site: %s", repr(site))
    site.setup()

    # run build
    site.build()


main()