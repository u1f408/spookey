# spookey

A simple static site generator, using Jinja2 for templating.

_**VERY MUCH STILL IN DEVELOPMENT DO NOT USE THIS YET**_

## Usage

Create a directory tree something like the following:

```
your-site/
|- _templates/
|  |- post.html
|  \- base.html
|
|- _posts/
|  |- 2020-09-04-hello-world.7
|  \- 2020-09-05-some-post.md
|
|- _config.yml
\- index.7
```

In `_config.yml`, place the following (adjusting fields as you wish):

```yaml
site_name: Your Site Name
baseurl: https://example.com

post:
  template: post.html
  permalink: /{year}/{slug}

page:
  template: page.html

exclude:
  - '\.git'
  - '\.swp'
```

Then, run:

```
$ spookey
```

There is no front matter on posts/pages.

For posts, only files matching the regexp
`\d{4}-\d{2}-\d{2}-[a-zA-Z0-9-]`
are handled, and the date of the post
is extracted from the filename.

For pages, every file is processed.

## Caveats

Currently the only supported format is mdoc(7).
Markdown support will be coming soon.

## License

MIT.
